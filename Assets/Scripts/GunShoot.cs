using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunShoot : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] float bulletForce;
    [SerializeField] Transform firePoint;
    ThirdPersonMovement movement;

    private void Start()
    {
        movement = GetComponent<ThirdPersonMovement>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && movement.isAiming)
        {
            GameObject shootB = Instantiate(bullet, firePoint.position, firePoint.rotation);
            Rigidbody bRB = shootB.GetComponent<Rigidbody>();
            bRB.AddForce(transform.forward * bulletForce);
            Destroy(shootB, 2f);
            Debug.Log("Pium");
        }
    }
}
