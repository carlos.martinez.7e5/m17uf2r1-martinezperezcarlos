using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ThirdPersonMovement : MonoBehaviour
{
    //El input del jugador
    PlayerInput playerInput;

    //Para controlar al personaje y la c�mara
    CharacterController controller;
    Camera cam;

    //Par�metros del personaje
    [SerializeField] float speed, jumpForce;
    float auxSpeed, runSpeed;
    Animator anim;

    //Para utilizar la pistola
    public bool isAiming = false;
    [SerializeField] GameObject pistola;

    //Para agacharse
    bool isCrouching = false;
    bool canMove = true;

    //Sinceramente, no se que hacen estas dos pero sin ellas no funciona correctamente
    float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

    //VARIABLES DE SUAVIZAR INPUT
    private Vector2 auxInput = Vector2.zero; //valor auxiliar que estar� suavizado y guiar� la animaci�n
    private float significativeDifference = 0.2f; // el suavizado se har� si hay una diferencia de 0,2 entre el valor auxiliar y el valor del input.
    private Vector2 input; //Vector2 con el resultado del input
    private Coroutine currentCorroutine = null; //variable donde se guarda la corrutina que activaremos

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
    }

    private void OnEnable()
    {
        playerInput.actions["Jump"].performed += ctx => Jump();
        playerInput.actions["Interact"].performed += ctx => PickUp();
        playerInput.actions["Emote"].performed += ctx => Dance();
    }

    private void OnDisable()
    {
        playerInput.actions["Jump"].performed -= ctx => Jump();
        playerInput.actions["Interact"].performed -= ctx => PickUp();
        playerInput.actions["Emote"].performed -= ctx => Dance();
    }

    private void Start()
    {
        Debug.Log(playerInput);

        //Bloquear el cursor dentro del juego
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        //Obtener referencias
        controller = GetComponent<CharacterController>();
        cam = GameObject.Find("Main Camera").GetComponent<Camera>();
        anim = GetComponent<Animator>();
        
        //Establecer valor a velocidad auxiliar, util para el movimiento
        auxSpeed = speed;
        runSpeed = speed * 2;
    }

    void Update()
    {
        Move();
        lockMouse();
        actionsAnimations();
    }

    void Move()
    {
        bool isGrounded = controller.isGrounded;

        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");
        input = new Vector2(h, v);
        if (isCrouching)
        {
            if ((auxInput - input).magnitude >= significativeDifference && currentCorroutine == null)
            //si la diferencia entre el auxiliar e input es m�s grande que 0.2 y no hay corrutina empezada
            {
                currentCorroutine = StartCoroutine(SmoothInput()); //guardamos en currentCorrutine la corrutina y la iniciamos
            }
        }

        Vector3 direction = new Vector3(h, 0f, v).normalized;
        Vector3 moveDir = new Vector3(h, 0.0f, v);

        if (direction.magnitude >= 0.1)
        {
            if (Input.GetKey(KeyCode.LeftShift)) speed = auxSpeed * 2;
            else speed = auxSpeed;

            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.transform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

            if (canMove) transform.rotation = Quaternion.Euler(0f, angle, 0f);

            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;

            moveDir = moveDir.normalized * speed * Time.deltaTime;
        }
        else speed = 0;

        //Gravedad
        float g = -9.81f * Time.deltaTime;
        moveDir.y = g;

        //Si aplico speed a todo el vector tambi�n afecta a la Y (subida y caida). Por eso m�s arriba multiplico X y Z arriba
        if (canMove) controller.Move(moveDir);

        //Animaciones
        if (isCrouching) anim.SetFloat("Speed", auxInput.magnitude * 7f / runSpeed); // 7 es el valor m�ximo de speed sin correr, no ponemos speed porque se har�a 0 al estar en pausa
        else anim.SetFloat("Speed", speed / runSpeed);
    }

    private IEnumerator SmoothInput()
    {
        auxInput = auxInput + (input - auxInput) * significativeDifference;
        //le sumamos la diferencia * 0,2, si input es mayor ser� positivo, sino le restar�.
        yield return new WaitForSeconds(0.01f);
        if ((auxInput - input).magnitude >= significativeDifference)
            //si sigue habiendo diferencia significativa, se vuelve a iniciar la corrutina (recursividad)
            yield return SmoothInput();
        //si no, ya no hay diferencia, decimos que la corrutina ha acabado, poniendo la variable a null
        //si m�s adelante se produce una diferencia por un cambio de input, se podr� activar la corrutina desde Move()
        currentCorroutine = null;
        yield return null; //acaba la corrutina
    }

    void Jump() { anim.SetTrigger("Jump"); }
    
    void Dance() { StartCoroutine(StaticAnimation("Dance")); }

    void PickUp() { StartCoroutine(StaticAnimation("PickUp")); }

    void actionsAnimations()
    {
        if (Input.GetKeyDown(KeyCode.H) && !isAiming) StartCoroutine(StaticAnimation("Dance"));

        if (Input.GetKeyDown(KeyCode.E)) StartCoroutine(StaticAnimation("PickUp"));
        
        if (Input.GetKeyDown(KeyCode.Space)) anim.SetTrigger("Jump");

        if (Input.GetMouseButtonDown(1) && !isCrouching) //Click "secundario" - Click derecho
        {
            if (!isAiming) StartCoroutine(DesenfundarArma(0.3f));
            else
            {
                AnimationClip enfundar = GetAnimation("EnfundarArma");
                StartCoroutine(EnfundarArma(enfundar.length + 0.2f));
            }

            isAiming = !isAiming;
            anim.SetBool("IsAiming", isAiming);
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            if (!isCrouching) StartCoroutine(StaticAnimation("IsCrouching"));
            
            isCrouching = !isCrouching;
            anim.SetBool("IsCrouching", isCrouching);
        }

        //Si se llama a alguna animaci�n mientras est� agachado, la "limpia" para que no la haga al ponerse de pie
        if (isCrouching) resetTriggers();
    }

    //"Limpia" los triggers que se llamen cuando no se pueden hacer al momento
    void resetTriggers()
    {
        anim.ResetTrigger("Dance");
        anim.ResetTrigger("PickUp");
        anim.ResetTrigger("Jump");
    }

    IEnumerator EnfundarArma(float time)
    {
        yield return new WaitForSeconds(time);
        pistola.SetActive(false);
    }

    IEnumerator DesenfundarArma(float time)
    {
        yield return new WaitForSeconds(time);
        pistola.SetActive(true);
    }

    //Intentar hacer una corrutina que tenga como par�metro una funci�n, y que pueda servir "universalente"

    IEnumerator StaticAnimation(string animationName)
    {
        anim.SetTrigger(animationName);
        canMove = false;

        AnimationClip staticAnim = GetAnimation(animationName);
        
        yield return new WaitForSeconds(staticAnim.length);
        canMove = true;
    }

    private AnimationClip GetAnimation(string animationName)
    {
        AnimationClip[] animClips = anim.runtimeAnimatorController.animationClips;
        AnimationClip clip = new AnimationClip();

        foreach (AnimationClip c in animClips) if (c.name == animationName) clip = c;

        return clip;
    }

    void lockMouse()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
