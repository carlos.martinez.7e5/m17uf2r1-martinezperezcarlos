INSTRUCCIONES:

WASD -> Movimiento (Caminar)
WASD + Shift -> Movimiento (Correr)
C -> Agacharse / dejar de agacharse
E -> Interactuar
Click derecho -> Apuntar / dejar de apuntar
Click izquierdo mientras apunta --> Disparar
Espacio -> Saltar
H -> Bailar

Ratón --> Controlar cámara

---------
Extras:
- Animación Desenfundar / enfundar
- New Input System ¿? [Implementado pero no funciona]