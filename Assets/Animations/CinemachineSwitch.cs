using System.Collections;
using UnityEngine;

public class CinemachineSwitch : MonoBehaviour
{
    Animator camAnimator;
    bool isAiming;
    bool isCrouching;

    private void Start()
    {
        camAnimator = GetComponent<Animator>();
        isAiming = false;
        isCrouching = false;
    }

    //No es la mejor manera de hacerlo, pero sin el new input es una manera que he encontrado

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C)) isCrouching = !isCrouching;

        if (Input.GetKeyDown(KeyCode.H) && !isAiming && !isCrouching) StartCoroutine(TauntAnim());

        if (Input.GetMouseButtonDown(1) && !isCrouching)
        {
            if (!isAiming) camAnimator.Play("AimingCamera");
            else camAnimator.Play("ThirdPersonCamera");

            isAiming = !isAiming;
        }
    }

    IEnumerator TauntAnim()
    {
        camAnimator.Play("TauntCamera");
        yield return new WaitForSeconds(12f);
        camAnimator.Play("ThirdPersonCamera");
    }
}
